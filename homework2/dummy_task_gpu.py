import torch

print('CUDA available: ', torch.version.cuda)
print('GPU available: ', torch.cuda.is_available())
print('GPU name: ', torch.cuda.get_device_name(torch.cuda.current_device()))

device = torch.device('cuda:0')
matrix1 = torch.ones([2,3], device=device)
matrix2 = torch.ones([3,2], device=device)

print(torch.matmul(matrix1, matrix2))
