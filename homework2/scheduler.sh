#!/usr/bin/zsh
#SBATCH --account=lect0096

### Start of Slurm SBATCH definitions
# GPUs per rank
#SBATCH --gres=gpu:1

# Number of tasks
#SBATCH --ntasks=1

# Memory per cpu
#SBATCH --mem-per-cpu=3900M

# Name the job
#SBATCH --job-name=NUMPY_DUMMY_JOB

# Declare file where the STDOUT/STDERR outputs will be written
#SBATCH --output=output.%J.log

### end of Slurm SBATCH definitions

### beginning of executable commands
source ../venv/bin/activate
nvidia-smi; echo
$MPIEXEC $FLAGS_MPI_BATCH python ./dummy_task_gpu.py
echo "test"
