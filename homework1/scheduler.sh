#!/usr/bin/zsh

### Start of Slurm SBATCH definitions
# Number of tasks
#SBATCH --ntasks=1

# Memory per cpu
#SBATCH --mem-per-cpu=3900M

# Name the job
#SBATCH --job-name=NUMPY_DUMMY_JOB

# Declare file where the STDOUT/STDERR outputs will be written
#SBATCH --output=output.%J.txt

### end of Slurm SBATCH definitions

### beginning of executable commands
### your program goes here
$MPIEXEC $FLAGS_MPI_BATCH python ./dummy_task.py
